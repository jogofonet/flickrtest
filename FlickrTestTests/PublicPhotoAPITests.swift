//
//  PublicPhotoAPITests.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import XCTest
@testable import FlickrTest

class PublicPhotoAPITests: XCTestCase {
    
    let publicPhotosAPI = PublicPhotosAPI(datasource:StubDatasource())
    

    func testGetPublicPhotos() {
        
        let expect = expectation(description: "Stubbed network call")
        
        publicPhotosAPI.getPublicPhotos { (publicPhotosResponse, error) in
            
            XCTAssertNil(error)
            XCTAssertNotNil(publicPhotosResponse)
            
            // NOTE: These tests are not extensive to save time
            
            XCTAssertEqual(publicPhotosResponse?.title, "Uploads from everyone")
            XCTAssert(publicPhotosResponse?.items?.count == 2)
            
            let item0 = publicPhotosResponse?.items?[0]
            XCTAssertEqual(item0?.title, "Vintage 90s Oversized Silk Shirt")
            
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
        
    }
    
    
    func testGetPublicPhotosWithTagQuery() {
        
        let tagString = "test tag #$%. 123 hyphen-word and,comma, separated"
        let expectedTags = "test,tag,123,hyphen,word,and,comma,separated"
        
        let tags = publicPhotosAPI.parseTagString(tagString)
        
        XCTAssertEqual(tags, expectedTags)
        
    }

    
}
