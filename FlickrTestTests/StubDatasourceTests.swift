//
//  StubDatasourceTests.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//


import XCTest
@testable import FlickrTest

class StubDatasourceTests: XCTestCase {
    
    
    func test200Response() {
        
        let handler = StubDatasource()
        let endpoint = APIEndpoint(path: "/test/path", stubPath: Bundle(for: type(of: self)).path(forResource: "StubDatasourceTests", ofType: "json") ?? "")
        let expect = expectation(description: "Stubbed network call")
        
        handler.request(endpoint) { (result) in
            
            switch result {
                
            case .success(let value):
                if let value = value as? [String: String] {
                    XCTAssert(value["result"] == "OK")
                } else {
                    XCTFail()
                }
                
                
            case .failure(_):
                XCTFail()
            }
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
}
