//
//  NetworkDatasourceTests.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//


import XCTest
import Mockingjay
@testable import FlickrTest

class NetworkDatasourceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        mockingjayRemoveStubOnTearDown = true
    }
    
    
    func test200Response() {
        
        let body = [ "some": "data" ]
        stub(uri("/test/path"), json(body))
        
        let handler = NetworkDatasource(baseUrl: "https://google.com")
        let endpoint = APIEndpoint(path: "/test/path", stubPath: "")
        let expect = expectation(description: "Stubbed network call")
        
        handler.request(endpoint) { (result) in
            
            switch result {
                
            case .success(let value):
                if let value = value as? [String: String] {
                    XCTAssert(value == body)
                } else {
                    XCTFail()
                }
                
                
            case .failure(_):
                XCTFail()
            }
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    
    
    func test500Response() {
        
        stub(uri("/test/path"), http(500))
        
        let handler = NetworkDatasource(baseUrl: "https://google.com")
        let endpoint = APIEndpoint(path: "/test/path", stubPath: "")
        let expect = expectation(description: "Stubbed network call")
        
        
        handler.request(endpoint) { (result) in
            
            switch result {
                
            case .success(_):
                XCTFail()
                
            case .failure(let error):
                XCTAssertNotNil(error)
            }
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 1.0, handler: nil)
        
    }
    
}
