//
//  StubDatasource.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import UIKit
import Alamofire

class StubDatasource {
    
    let responseDelay = 0.2
    

}


extension StubDatasource : APIDatasource {
    
    @discardableResult
    func request(
        _ endpoint: APIEndpoint,
        handler: @escaping (APIResult<[String: Any]>) -> Void)
        
        -> APIRequest? {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + responseDelay) { 
                
                let url = URL(fileURLWithPath: endpoint.stubPath)
                
                do {
                    let data = try Data(contentsOf: url, options: Data.ReadingOptions.mappedIfSafe)
                    let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    
                    if let dict = dict {
                        handler(APIResult.success(dict))
                    } else {
                        handler(APIResult.failure(NSError(domain: "StubDomain", code: 1, userInfo: nil)))
                    }
                    
                    
                    
                } catch {
                    
                    print("Error reading stub file: \(error)")
                    handler(APIResult.failure(error))
                }
                
            }
            
            
            
            return nil
            
    }
    
}
