//
//  PublicPhotosMedia.swift
//
//  Created by Jonathon Francis on 11/7/17
//  Copyright (c) JoGoFo. All rights reserved.
//

import Foundation
import ObjectMapper

public final class PublicPhotosMedia: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let m = "m"
  }

  // MARK: Properties
  public var m: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    m <- map[SerializationKeys.m]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = m { dictionary[SerializationKeys.m] = value }
    return dictionary
  }

}
