//
//  NetworkDatasource.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import UIKit
import Alamofire

class NetworkDatasource {
    
    var baseUrl : String
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func url(forPath path: String) -> URL? {
        
        let url = URL(string:baseUrl)?.appendingPathComponent(path)
        return url
        
    }
    
    func alamoHTTPMethod(for method: HTTPReqMethod) -> HTTPMethod {
        switch method {
        case .get: return HTTPMethod.get
        case .post: return HTTPMethod.post
        }
    }
}

extension NetworkDatasource: APIDatasource {
    
    @discardableResult
    func request(
        _ endpoint: APIEndpoint,
        handler: @escaping (APIResult<[String: Any]>) -> Void)
        
        -> APIRequest? {
            
            guard let url = self.url(forPath: endpoint.path) else {
                handler(APIResult.failure(NetworkError.invalidDomain))
                return nil
            }
            
            let request = Alamofire.request(url, method: alamoHTTPMethod(for: endpoint.method), parameters: endpoint.parameters, headers: endpoint.headers)
                .validate()
                .responseFlickrJSON { response in
                    
                    if response.error?._domain == NSURLErrorDomain,
                        response.error?._code == NSURLErrorCancelled {
                        // Request was cancelled, do not call handler
                        return
                    }
                    
                    switch response.result {
                    case .success(let value as [String: Any]):
                        handler(APIResult<[String: Any]>.success(value))
                    case .failure(let error):
                        handler(APIResult.failure(error))
                    default:
                        handler(APIResult.failure(NetworkError.unknownError))
                    }
                }
            
            return request
    }

}

enum NetworkError : Error {
    case unknownError
    case invalidDomain
}

// DataRequest already implements all necessary methods
extension DataRequest : APIRequest {
}




extension DataRequest {
    /// Creates a response serializer that returns a JSON object result type constructed from the response data using
    /// `JSONSerialization` with the specified reading options and allowing for Flickr's invalid JSON responses
    ///
    /// - parameter options: The JSON serialization reading options. Defaults to `.allowFragments`.
    ///
    /// - returns: A JSON object response serializer.
    public static func flickrJsonResponseSerializer(
        options: JSONSerialization.ReadingOptions = .allowFragments)
        -> DataResponseSerializer<Any>
    {
        return DataResponseSerializer { _, response, data, error in
            
            var fixedData : Data? = data
            
            // Flickr sends single quote (') characters as an escaped character (\') which is invalid JSON
            if let data = data {
                let dataString = String(data: data, encoding: .utf8)
                let fixedString = dataString?.replacingOccurrences(of: "\\'", with: "'")
                fixedData = fixedString?.data(using: .utf8)
            }
            
            return Request.serializeResponseJSON(options: options, response: response, data: fixedData, error: error)
        }
    }
    
    /// Adds a handler for the dealing with the invalid JSON responses from flickr, to be called once the request has finished.
    ///
    /// - parameter options:           The JSON serialization reading options. Defaults to `.allowFragments`.
    /// - parameter completionHandler: A closure to be executed once the request has finished.
    ///
    /// - returns: The request.
    @discardableResult
    public func responseFlickrJSON(
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        completionHandler: @escaping (DataResponse<Any>) -> Void)
        -> Self
    {
        return response(
            queue: queue,
            responseSerializer: DataRequest.flickrJsonResponseSerializer(options: options),
            completionHandler: completionHandler
        )
    }
}
