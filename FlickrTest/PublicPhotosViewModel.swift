//
//  PublicPhotosViewModel.swift
//  FlickrTest
//
//  Created by JoGoFo on 2/8/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol PublicPhotosViewModelProtocol : class {
    
    var photos : MutableProperty<[PhotoViewModelProtocol]> { get }
    var isRefreshing : MutableProperty<Bool> { get }
    var errorMessage : MutableProperty<String?> { get }
    
    func refreshData(tagQuery : String?)
    
}

class PublicPhotosViewModel : PublicPhotosViewModelProtocol {

    let publicPhotosAPI : PublicPhotosAPI
    var photos = MutableProperty<[PhotoViewModelProtocol]>([PhotoViewModelProtocol]())
    var isRefreshing = MutableProperty<Bool>(false)
    var errorMessage = MutableProperty<String?>(nil)
    
    init(publicPhotosAPI: PublicPhotosAPI) {
        self.publicPhotosAPI = publicPhotosAPI
    }
    
    private var activeRequest : APIRequest? = nil
    
    func refreshData(tagQuery: String? = nil) {
        
        if activeRequest != nil {
            activeRequest?.cancel()
            activeRequest = nil
        }
        
        photos.value = [PhotoViewModel]()
        isRefreshing.value = true
        errorMessage.value = nil
        
        activeRequest = publicPhotosAPI.getPublicPhotos(tagQuery: tagQuery) { [unowned self] (publicPhotosResponse, error) in
            
            self.isRefreshing.value = false
            
            guard error == nil else {
                self.errorMessage.value = "Oops, something went wrong! Please check your internet connection and try again"
                return
            }
            
            if let responseObject = publicPhotosResponse {
            
                var photos = [PhotoViewModel]()

                responseObject.items?.forEach({ item in
                    photos.append(PhotoViewModel(photo: item))
                })
                
                self.photos.value = photos
            }

        }
    }
    
    
}
