//
//  APIDatasource.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 JoGoFo. All rights reserved.
//

import Foundation

enum APIResult<T> {
    case success(T)
    case failure(Error)
}

struct APIEndpoint {
    let path : String
    let stubPath : String
    let method : HTTPReqMethod
    let parameters : [String: Any]?
    let headers : [String: String]?
    
    init(path: String,
         stubPath: String,
         method: HTTPReqMethod = .get,
         parameters: [String: Any]? = nil,
         headers : [String: String]? = nil) {
        
        self.path = path
        self.stubPath = stubPath
        self.method = method
        self.parameters = parameters
        self.headers = headers
    }
}


enum HTTPReqMethod {
    case get
    case post
}


protocol APIDatasource {
    
    @discardableResult
    func request(
        _ endpoint: APIEndpoint,
        handler: @escaping (APIResult<[String: Any]>) -> Void)
        
        -> APIRequest?
}

protocol APIRequest {
    func cancel()
}
