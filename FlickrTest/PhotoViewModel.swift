//
//  Photo.swift
//  FlickrTest
//
//  Created by JoGoFo on 1/8/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import Foundation

protocol PhotoViewModelProtocol : class {
    
    var title : String? { get }
    
    func getImageData(_ handler: @escaping (Data) -> Void)
    
}

class PhotoViewModel : PhotoViewModelProtocol {
    
    private let photo : PublicPhotosItem
    
    var title : String?
    
    func getImageData(_ handler: @escaping (Data) -> Void) {
        
        if let urlString = photo.media?.m {
        
            CachedImageAPI.getImageData(urlString) { imageData in
                handler(imageData)
            }
            
        }
    }
    
    init(photo: PublicPhotosItem) {
        self.photo = photo
        self.title = photo.title
    }
    
}
