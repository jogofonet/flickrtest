//
//  PublicPhotosItem.swift
//
//  Created by Jonathon Francis on 11/7/17
//  Copyright (c) JoGoFo. All rights reserved.
//

import Foundation
import ObjectMapper

public final class PublicPhotosItem: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let published = "published"
    static let link = "link"
    static let media = "media"
    static let dateTaken = "date_taken"
    static let authorId = "author_id"
    static let title = "title"
    static let descriptionValue = "description"
    static let author = "author"
    static let tags = "tags"
  }

  // MARK: Properties
  public var published: String?
  public var link: String?
  public var media: PublicPhotosMedia?
  public var dateTaken: String?
  public var authorId: String?
  public var title: String?
  public var descriptionValue: String?
  public var author: String?
  public var tags: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    published <- map[SerializationKeys.published]
    link <- map[SerializationKeys.link]
    media <- map[SerializationKeys.media]
    dateTaken <- map[SerializationKeys.dateTaken]
    authorId <- map[SerializationKeys.authorId]
    title <- map[SerializationKeys.title]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    author <- map[SerializationKeys.author]
    tags <- map[SerializationKeys.tags]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = published { dictionary[SerializationKeys.published] = value }
    if let value = link { dictionary[SerializationKeys.link] = value }
    if let value = media { dictionary[SerializationKeys.media] = value.dictionaryRepresentation() }
    if let value = dateTaken { dictionary[SerializationKeys.dateTaken] = value }
    if let value = authorId { dictionary[SerializationKeys.authorId] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = author { dictionary[SerializationKeys.author] = value }
    if let value = tags { dictionary[SerializationKeys.tags] = value }
    return dictionary
  }

}
