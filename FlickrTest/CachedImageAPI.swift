//
//  CachedImageAPI.swift
//  FlickrTest
//
//  Created by JoGoFo on 16/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import Foundation

class CachedImageAPI {

    private static let cache: NSCache = { () -> NSCache<NSString, NSData> in
        let cache = NSCache<NSString, NSData>()
        cache.name = "TSImageCache"
        cache.countLimit = 50
        cache.totalCostLimit = 20*1024*1024
        return cache
    }()
    
    
    class func getImageData(_ urlString: String, handler: @escaping (Data) -> Void) {
    
        let imageData = CachedImageAPI.cache.object(forKey: urlString as NSString)
        
        // return cached image
        if let imageData = imageData as Data? {
            handler(imageData)
            return
        }
        
        let url = URL(string: urlString)
        
        guard url != nil else { return }
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            
            DispatchQueue.main.async {
                if let data = data {
                    
                    CachedImageAPI.cache.setObject(
                        data as NSData,
                        forKey: urlString as NSString,
                        cost: data.count)
                    
                    handler(data)
                }
            }
        }
    }

}
