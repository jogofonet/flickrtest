//
//  PublicPhotosPublicPhotosResponse.swift
//
//  Created by Jonathon Francis on 11/7/17
//  Copyright (c) JoGoFo. All rights reserved.
//

import Foundation
import ObjectMapper

public final class PublicPhotosResponse: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let items = "items"
        static let descriptionValue = "description"
        static let title = "title"
        static let generator = "generator"
        static let modified = "modified"
        static let link = "link"
    }
    
    // MARK: Properties
    public var items: [PublicPhotosItem]?
    public var descriptionValue: String?
    public var title: String?
    public var generator: String?
    public var modified: String?
    public var link: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public required init?(map: Map){
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        items <- map[SerializationKeys.items]
        descriptionValue <- map[SerializationKeys.descriptionValue]
        title <- map[SerializationKeys.title]
        generator <- map[SerializationKeys.generator]
        modified <- map[SerializationKeys.modified]
        link <- map[SerializationKeys.link]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = items { dictionary[SerializationKeys.items] = value.map { $0.dictionaryRepresentation() } }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = generator { dictionary[SerializationKeys.generator] = value }
        if let value = modified { dictionary[SerializationKeys.modified] = value }
        if let value = link { dictionary[SerializationKeys.link] = value }
        return dictionary
    }
    
}
