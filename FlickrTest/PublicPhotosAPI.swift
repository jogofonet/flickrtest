//
//  PublicPhotosAPI.swift
//  FlickrTest
//
//  Created by JoGoFo on 11/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import Foundation

class PublicPhotosAPI {
    
    let datasource : APIDatasource
    
    init(datasource: APIDatasource) {
        self.datasource = datasource
    }
    
    
    @discardableResult
    func getPublicPhotos(tagQuery: String? = nil, handler: @escaping (PublicPhotosResponse?, Error?) -> Void) -> APIRequest? {
        
        var parameters = ["format" : "json", "nojsoncallback" : "1"]
        
        if let tagQuery = tagQuery {
            parameters["tags"] = parseTagString(tagQuery)
        }
        
        let endpoint = APIEndpoint(path: "/services/feeds/photos_public.gne",
                                   stubPath: Bundle(for: type(of: self)).path(forResource: "GetPublicPhotosStub", ofType: "json") ?? "",
                                   parameters: parameters,
                                   headers: ["Content-Type" : "application/json"])
        
        return datasource.request(endpoint) { (result) in
            
            switch result {
                
            case .success(let value):
                
                if let responseObject = PublicPhotosResponse(JSON: value) {
                    
                    handler(responseObject, nil)
                    
                } else {
                    handler(nil, APIError.modelParsingError)
                }
                
            case .failure(let error):
                handler(nil, error)
                
                
            }
        }
    }
    
    
    func parseTagString(_ tagString: String) -> String {
        
        let allowedCharacters = CharacterSet.alphanumerics.union(CharacterSet(charactersIn: "_"))
        let disallowedCharacters = allowedCharacters.inverted
        
        let strippedString = tagString.components(separatedBy: disallowedCharacters)
            .filter{ !$0.isEmpty }
            .joined(separator: ",")

        return strippedString
    }
}

enum APIError : Error {
    case modelParsingError
}
