//
//  PhotoThumbnailCollectionViewCell.swift
//  FlickrTest
//
//  Created by JoGoFo on 12/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import UIKit

class PhotoThumbnailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!
    
    var photo : PhotoViewModelProtocol? {
        didSet {
            
            guard let photo = photo else {
                clearCell()
                return
            }
            
            setMeta(forPhoto: photo)
            
            imageView.image = nil
            
            photo.getImageData({ [unowned self] imageData in
                guard self.photo === photo else {
                    return  // Cell may have been reused
                }
                
                self.imageView.image = UIImage(data: imageData)
            })
            
        }
    }
    
    
    override func awakeFromNib() {
        clearCell()
    }
    
    private func clearCell() {
        imageView.image = nil
        gradientView.isHidden = true
        titleLabel.isHidden = true
    }
    
    private func setMeta(forPhoto photo: PhotoViewModelProtocol) {
        
        gradientView.isHidden = false
        titleLabel.isHidden = false
        
        titleLabel.text = photo.title
        
    }
    
}


class metaDataGradientView : UIView {
    
    override func awakeFromNib() {
        
        var gl: CAGradientLayer
        
        let colorTop = UIColor(white: 0, alpha: 0).cgColor
        let colorBottom = UIColor(white: 0, alpha: 0.3).cgColor
        
        gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        
        gl.frame = bounds
        backgroundColor = UIColor.clear
        layer.insertSublayer(gl, at: 0)
    }
    
}
