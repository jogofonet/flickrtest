//
//  PublicPhotosViewController.swift
//  FlickrTest
//
//  Created by JoGoFo on 14/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class PublicPhotosViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var errorLabel: UILabel!
    
    var publicPhotosViewModel : PublicPhotosViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.reactive.text <~ publicPhotosViewModel.errorMessage
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "CollectionViewController",
            let vc = segue.destination as? PublicPhotosCollectionViewController {
            vc.publicPhotosViewModel = publicPhotosViewModel
        }
    }


    @IBAction func refreshButtonAction(_ sender: Any) {
        publicPhotosViewModel.refreshData(tagQuery: searchBar.text)
    }
}

extension PublicPhotosViewController : UISearchBarDelegate {
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchString = searchBar.text
        
        publicPhotosViewModel.refreshData(tagQuery: searchString)
        
        searchBar.resignFirstResponder()
    }
}

