//
//  PublicPhotosCollectionViewController.swift
//  FlickrTest
//
//  Created by JoGoFo on 12/7/17.
//  Copyright © 2017 Jonathon Francis. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import ImageViewer

private let reuseIdentifier = "PhotoThumbnameCellReuseIdentifier"

class PublicPhotosCollectionViewController: UICollectionViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var publicPhotosViewModel : PublicPhotosViewModelProtocol!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.reactive.isAnimating <~ publicPhotosViewModel.isRefreshing
        
        if let collectionView = collectionView {
            collectionView.reactive.reloadData <~ publicPhotosViewModel.photos.map { _ in () }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        publicPhotosViewModel.refreshData(tagQuery: nil)
    }


    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return publicPhotosViewModel.photos.value.count 
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        if let cell = cell as? PhotoThumbnailCollectionViewCell {
            cell.photo = publicPhotosViewModel.photos.value[indexPath.row]
        }
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let config = [
            
            GalleryConfigurationItem.closeButtonMode(.builtIn),
            GalleryConfigurationItem.thumbnailsButtonMode(.none),
            GalleryConfigurationItem.deleteButtonMode(.none)
        ]
        
        let galleryViewController = GalleryViewController(startIndex: indexPath.row, itemsDataSource: self, configuration: config)

        galleryViewController.landedPageAtIndexCompletion = { index in
            self.collectionView?.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredVertically, animated: false)
        }

        self.presentImageGallery(galleryViewController)
        
    }

}


fileprivate let minCellWidth : CGFloat = 100.0
fileprivate let cellSpacing : CGFloat = 6.0

extension PublicPhotosCollectionViewController : UICollectionViewDelegateFlowLayout {

    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // Fits the cell within the view allowing for spacing, adhering to the minCellWidth
        let availableWidth = collectionView.bounds.width - cellSpacing
        let minCellWidthWithPadding = minCellWidth + cellSpacing
        let cellsPerRow = floor(availableWidth / minCellWidthWithPadding)
        let remainingWidth = availableWidth - (cellsPerRow * minCellWidthWithPadding)
        
        let widthPerCell = (remainingWidth / cellsPerRow) + minCellWidth
        
        return CGSize(width: widthPerCell, height: widthPerCell)
    }
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 30.0, left: cellSpacing, bottom: 30.0, right: cellSpacing)
    }
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                        collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
}



extension PublicPhotosCollectionViewController: GalleryItemsDataSource {
    
    func itemCount() -> Int {
        
        return publicPhotosViewModel.photos.value.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        
        return .image(fetchImageBlock: { [unowned self] completionBlock in
            
            self.publicPhotosViewModel.photos.value[index].getImageData({ imageData in
                completionBlock(UIImage(data: imageData))
            })
        
        })
    }
}

